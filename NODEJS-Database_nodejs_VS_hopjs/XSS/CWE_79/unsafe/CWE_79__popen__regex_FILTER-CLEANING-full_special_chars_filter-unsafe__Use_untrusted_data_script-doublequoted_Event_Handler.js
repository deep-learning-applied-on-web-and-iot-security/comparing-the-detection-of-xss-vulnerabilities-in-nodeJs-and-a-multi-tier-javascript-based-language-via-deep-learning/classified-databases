/* 
Unsafe sample
input : Uses popen to read the file /tmp/tainted.txt using cat command
Uses a full_special_chars_filter via filter_var function
File : use of untrusted data in a double quoted event handler in a script
*/



/*
MIT License

Copyright (c) 2021 MAUREL Héloïse

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/



var http_var = require( 'http' ) ;

var server = http_var.createServer( function( $req , res_var ) {
    res_var.writeHead( 200 , { 'Content-Type' : 'text/html' } ) ;
    res_var.write( "<!DOCTYPE html>" +
             "<html>" +
             "<head>" +
             "<title> XSS </title>" +
             "</head>" +
             "<body>"
            ) ;
  var $cp = require( 'child_process' );
  var $buff = $cp.execSync( '/bin/cat /tmp/tainted.txt' );
  var $tainted = $buff.toString();
    


                var $map_var = { '&' : "&amp;" , '<' : "&lt;" , '>' : "&gt;" , '"' : "&quot;" , "'" : "&apos;" };
                var $re = /[&<>"']/g ;
                var $sanitized = $tainted;
                $tainted = $tainted.replace( $re , function( $m ) { return $map_var[ $m ] ; } ) ;
                $tainted = $sanitized ;
            

	//flaw

    res_var.write( "<div onmouseover = \"x=\"" + $tainted + "\"\>" ) ;
    res_var.write(
                "<h1>Hello World!</h1>" +
                "</div>" +
                "</body>" +
                "</html>" ) ;
    res_var.end() ;
} ) ;
server.listen( 8080 ) ;
