/* 
Safe sample
input : get the field userData from the variable $_GET via an object, which store it in a array
Flushes content of $sanitized if the filter number_int_filter is not applied
File : unsafe, use of untrusted data in an tag name
*/



/*
MIT License

Copyright (c) 2021 MAUREL Héloïse

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/



var http_var = require( 'http' ) ;

var server = http_var.createServer( function( $req , res_var ) {
    res_var.writeHead( 200 , { 'Content-Type' : 'text/html' } ) ;
    res_var.write( "<!DOCTYPE html>" +
             "<html>" +
             "<head>" +
             "<title> XSS </title>" +
             "</head>" +
             "<body>"
            ) ;
    var url_var = require('url');
    class Input {
        constructor() {
            this.$input = {
                $test : 'safe' ,
                $realOne : url_var.parse( $req.url_var , true).query.userData ,
                $trap : 'safe'
            };
        }

        getInput() {
            return this.$input.$realOne ;
        }
    }
    var $temp = new Input() ;
    var $tainted = $temp.getInput() ;
    


    function number_int_validation( str_var ) {
        var $integer_validation = /^(\+|-)?\d+$/g ;
        if($integer_validation.test( str_var )){
            return true;
        }
        else{
            return false;
        }
    }
    $sanitized = $tainted;
    if( number_int_validation( $sanitized ) ){
        $tainted = $sanitized ;
    }
    else{
        $tainted = "" ;
    }
            

    res_var.write( "<" + $tainted + " href=\"/bob\" />" ) ;
    res_var.write(
               "<h1>Hello World!</h1>" +
               "</body>" +
               "</html>" ) ;
    res_var.end() ;
} ) ;
server.listen( 8080 ) ;