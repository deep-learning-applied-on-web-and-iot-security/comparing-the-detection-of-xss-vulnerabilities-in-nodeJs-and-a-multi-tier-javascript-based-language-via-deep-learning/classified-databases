/* 
Safe sample
input : backticks interpretation, reading the file /tmp/tainted.txt
SANITIZE : use in_array to check if $tainted is in the white list
File : unsafe, use of untrusted data in a script
*/



/*
MIT License

Copyright (c) 2021 MAUREL Héloïse

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/



var http_var = require( 'http' ) ;

var server = http_var.createServer( function( $req , res_var ) {
    res_var.writeHead( 200 , { 'Content-Type' : 'text/html' } ) ;
    res_var.write( "<!DOCTYPE html>" +
             "<html>" +
             "<head>" +
             "<title> XSS </title>" +
             "<script>"
            ) ;
  var $cmd = ` cat /tmp/tainted.txt ` ;
  var { execSync } = require( 'child_process' ) ;
  var $tainted = execSync( $cmd ).toString() ;
    


            var $legal_table = new Array( "1" , "safe2" ) ;
            if( $legal_table.includes( $tainted ) ) {
                $tainted = $tainted ;
            } else {
                $tainted = $legal_table[ 0 ] ;
            }
            

    res_var.write( $tainted ) ;
    res_var.write( "</script>" +
              "</head>" +
               "<body>" +
               "<h1>Hello World!</h1>" +
               "</body>" +
               "</html>" ) ;
    res_var.end() ;
} ) ;
server.listen( 8080 ) ;
