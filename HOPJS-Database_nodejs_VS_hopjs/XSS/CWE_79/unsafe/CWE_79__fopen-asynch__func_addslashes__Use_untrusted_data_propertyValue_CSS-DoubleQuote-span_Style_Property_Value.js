/* 
Unsafe sample
input : use fopen to read /tmp/tainted.txt and put the first line in $tainted
sanitize : use of the function addslashes
File : unsafe, use of untrusted data in a property value in a span tag(CSS)
*/



/*
MIT License

Copyright (c) 2021 MAUREL Héloïse

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


'use hopscript';
service server( { UserData : userData } ) {
    var head_var = <head> <title> XSS </title> </head> ;
    var body_var = <body> <h1> Hello World! </h1> </body> ;

    var $filename = "/tmp/tainted.txt" ;
    function readfile_func( file_var ){
        const $fs_var = require( 'fs' ) ;
        return new Promise( ( resolve, reject ) => {
            $fs_var.readFile( file_var , 'utf8', ( error_var, stdout_var, stderr_var ) => {
                if( error_var ) {
                    console.warn( error_var ) ;
                }
                resolve( stdout_var ? stdout_var : stderr_var ) ;
            } ) ;
        } ) ;
    }
    return ( async function( param_var ){
        var $tainted = await readfile_func( param_var );
            


            function addslashes ( str_var ) {
                return ( str_var + '' ).replace( /[\\"']/g , '\\$&' ).replace( /\u0000/g , '\\0' ) ;
            }
            var $sanitized = addslashes( $tainted ) ;
            $tainted = $sanitized ;
            

	//flaw
    body_var.appendChild( "<span style = \"color :" + $tainted + "\"> Hey </span>" ) ;
    return <html> ${ head_var } ${ body_var } </html> ;
}
    ) ( $filename ) ;
}
            

