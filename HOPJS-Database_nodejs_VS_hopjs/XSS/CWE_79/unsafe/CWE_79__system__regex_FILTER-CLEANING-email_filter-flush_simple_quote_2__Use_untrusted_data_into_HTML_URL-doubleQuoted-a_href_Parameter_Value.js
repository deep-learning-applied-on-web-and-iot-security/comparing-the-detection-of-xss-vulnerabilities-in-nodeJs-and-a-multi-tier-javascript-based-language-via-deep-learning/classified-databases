/* 
Unsafe sample
input : execute a ls command using the function system, and put the last result in $tainted
Uses an email_filter via filter_var function
File : unsafe, use of untrusted data in a property value in a span tag(CSS)
*/



/*
MIT License

Copyright (c) 2021 MAUREL Héloïse

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


'use hopscript';
service server( { UserData : userData } ) {
    var head_var = <head> <title> XSS </title> </head> ;
    var body_var = <body> <h1> Hello World! </h1> </body> ;
    var { execSync } = require( 'child_process ');
    var $tainted = execSync( "ls" );
        


            var $re = /(\.{2,}|[`~!#$%^&*()|+\-=?;:'",<>\{\}\[\]\\\/ ])/gi ;
            var $email_re =
            /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ ;
            $tainted = $tainted.replace( $re , '' ) ;
            if ( $email_re.test( $tainted ) ){
                $tainted = $tainted ;
            }
            else{
                $tainted = "" ;
            }
            

	//flaw
    body_var.appendChild(   "<a href=\"" + $tainted  + "\">link</a>" ) ;
    return <html> ${ head_var } ${ body_var } </html> ;
}
