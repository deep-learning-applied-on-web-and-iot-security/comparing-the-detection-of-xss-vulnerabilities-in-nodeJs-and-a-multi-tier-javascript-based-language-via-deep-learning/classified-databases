/* 
Unsafe sample
input : use exec to execute the script /tmp/tainted.php and store the output in $tainted
Flushes content of $sanitized if the filter email_filter is not applied
File : unsafe, use of untrusted data in a function in a script
*/



/*
MIT License

Copyright (c) 2021 MAUREL Héloïse

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


'use hopscript';
service server( { UserData : userData } ) {
    var head_var = <head> <title> XSS </title> </head> ;
    var script_var = <script/> ;
    var func_var = 'function xss(){';
    var $script = "/tmp/tainted.js" ;
    function forkLaunchScript( cmd_var ) {
        const $cp = require( 'child_process' );
        return new Promise(( resolve , reject ) => {
            var $compute_var = $cp.fork(cmd_var );
            $compute_var.send('start');
            var $tainted_var = "";

            $compute_var.on('message', (data_var) => {
                console.log( `Child exited with code ${data_var} ` ) ;
                $tainted_var = data_var;
                resolve( $tainted_var );
            } ) ;

            $compute_var.on('error', (data_var) => {
                console.log( `Child exited with code ${data_var} ` ) ;
                $tainted_var = data_var;
                reject( $tainted_var );
            } ) ;
        } ) ;
    }

    return ( async function( param_var ) {
        var $tainted = await forkLaunchScript( param_var ) ;
            


            var $email_re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ ;
            var $sanitized = $tainted ;
            if( $email_re.test( $sanitized ) ){
                $tainted = $sanitized ;
            }
            else{
                $tainted = "" ;
            }
            

	//flaw
    func_var = func_var + $tainted + '}';
    script_var.appendChild( func_var ) ;
    var body_var = <body onload=~{xss()}>
    <h1> Hello World! </h1>
    </body> ;
    return <html> ${ head_var } ${ script_var } ${ body_var } </html> ;
}

     ) ( $script );
}
            

