/* 
Unsafe sample
input : use fopen to read /tmp/tainted.txt and put the first line in $tainted
Uses a magic_quotes_filter via filter_var function
File : use of untrusted data in a double quoted property value (CSS)
*/



/*
MIT License

Copyright (c) 2021 MAUREL Héloïse

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


'use hopscript';
service server( { UserData : userData } ) {
    var head_var = <head> <title> XSS </title> </head> ;
    var style_var = '<style> ';
    var $fs_var = require( 'fs' ) ;
    var $filename = "/tmp/tainted.txt" ;
    var $tainted = $fs_var.readFileSync( $filename , 'utf8' ) ;
        


                $tainted = ( $tainted + '' ).replace( /[\\"']/g , '\\$&' ).replace( /\u0000/g , '\\0') ;
            

	//flaw
    style_var = style_var + "body { color : \"" + $tainted + "\" ; }" + '</style>' ;
    var body_var = <body>
    <h1> Hello World! </h1>
    </body> ;
    body_var.appendChild( style_var ) ;
    return <html> ${ head_var } ${ body_var } </html> ;
}