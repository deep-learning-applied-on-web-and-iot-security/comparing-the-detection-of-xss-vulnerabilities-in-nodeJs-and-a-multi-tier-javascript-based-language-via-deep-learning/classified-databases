/* 
Unsafe sample
input : backticks interpretation, reading the file /tmp/tainted.txt
sanitize : use of the function htmlentities. Sanitizes the query but has a high chance to produce unexpected results
File : unsafe, use of untrusted data in a comment
*/



/*
MIT License

Copyright (c) 2021 MAUREL Héloïse

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


'use hopscript';
service server( { UserData : userData } ) {
    var head_var = <head> <title> XSS </title> </head> ;
    var func_var = '<!--';
    var $cmd = ` cat /tmp/tainted.txt ` ;
    function execShellCommand( cmd_var ) {
        const exec = require('child_process').exec;
        return new Promise( ( resolve, reject ) => {
            exec( cmd_var , ( error_var, stdout_var, stderr_var ) => {
                if( error_var ) {
                    console.warn( error_var );
                }
                resolve( stdout_var ? stdout_var : stderr_var ) ;
            } ) ;
        } ) ;
    }

    return ( async function( param_var ) {
        var $tainted = await execShellCommand( param_var ) ;
            


                function htmlEntities( str_var ) {
                    return String( str_var ).replace( /&/g , '&' ).replace( /</g , '<' ).replace( />/g , '>' ).replace( /"/g ,
                '"' ) ;
                }
                $tainted = htmlEntities( $tainted ) ;
            

	//flaw
    func_var = func_var + $tainted + ' -->' ;
    head_var.appendChild( func_var ) ;
    var body_var = <body>
    <h1> Hello World! </h1>
    </body> ;
    return <html> ${ head_var } ${ body_var } </html> ;
}
    )( $cmd );
}
            

