/* 
Safe sample
input : get the field userData from the variable $_GET via an object, which store it in a array
Flushes content of $sanitized if the filter number_int_filter is not applied
File : use of untrusted data in a quoted event handler in a script
*/



/*
MIT License

Copyright (c) 2021 MAUREL Héloïse

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


'use hopscript';
service server( { UserData : userData } ) {
    var head_var = <head> <title> XSS </title> </head> ;
    var body_var = <body> <h1> Hello World! </h1> </body> ;

    var url_var = require( 'url' );
    var path_var = this.path;
    class Input {
        constructor() {
        this.$input = new Array();
        this.$input.push( 'safe' );

        this.$input.push( url_var.parse( path_var , true ).query.userData );
        this.$input.push( 'safe' );

        }

        getInput() {
            return this.$input[ 1 ] ;
        }
    }
    var $temp = new Input() ;
    var $tainted = $temp.getInput() ;
        


            var $integer_replace = /[^0-9+-]/g;
            var $integer_validation = /^(\+|-)?\d+$/g ;
            var $sanitized = $tainted.replace( $integer_replace , '' ) ;
            if( $integer_validation.test( $sanitized ) ){
                $tainted = $sanitized ;
            }
            else{
                $tainted = "" ;
            }
            

    body_var.appendChild( "<div onmouseover = \"x='" + $tainted + "'\>" ) ;
    return <html> ${ head_var } ${ body_var } </html> ;
}


