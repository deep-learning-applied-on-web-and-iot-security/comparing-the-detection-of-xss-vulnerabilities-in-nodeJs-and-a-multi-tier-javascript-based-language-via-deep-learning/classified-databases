/* 
Safe sample
input : use exec to execute the script /tmp/tainted.php and store the output in $tainted
sanitize : use of ternary condition
File : use of untrusted data in a div tag
*/



/*
MIT License

Copyright (c) 2021 MAUREL Héloïse

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


'use hopscript';
service server( { UserData : userData } ) {
    var head_var = <head> <title> XSS </title> </head> ;
    var body_var = <body>
    <h1> Hello World! </h1>
    </body> ;
    var div_var = <div/> ;
    var $script = "/tmp/tainted.js" ;
    function forkLaunchScript( cmd_var ) {
        const $cp = require( 'child_process' );
        return new Promise(( resolve , reject ) => {
            var $compute_var = $cp.fork(cmd_var );
            $compute_var.send('start');
            var $tainted_var = "";

            $compute_var.on('message', (data_var) => {
                console.log( `Child exited with code ${data_var} ` ) ;
                $tainted_var = data_var;
                resolve( $tainted_var );
            } ) ;

            $compute_var.on('error', (data_var) => {
                console.log( `Child exited with code ${data_var} ` ) ;
                $tainted_var = data_var;
                reject( $tainted_var );
            } ) ;
        } ) ;
    }

    return ( async function( param_var ) {
        var $tainted = await forkLaunchScript( param_var ) ;
            

$tainted = $tainted  == 1 + "-1.3e3" ? 1 + "-1.3e3" : 'safe2' ;

    div_var.appendChild( $tainted ) ;
    body_var.appendChild( div_var ) ;
    return <html> ${ head_var } ${ body_var } </html> ;
}
     ) ( $script );
}
            

