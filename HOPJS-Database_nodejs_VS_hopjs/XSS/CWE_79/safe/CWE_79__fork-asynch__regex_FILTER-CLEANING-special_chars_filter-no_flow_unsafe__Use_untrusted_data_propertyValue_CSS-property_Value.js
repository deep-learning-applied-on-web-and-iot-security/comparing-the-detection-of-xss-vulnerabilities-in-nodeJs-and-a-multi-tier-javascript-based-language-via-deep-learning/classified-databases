/* 
Safe sample
input : use exec to execute the script /tmp/tainted.php and store the output in $tainted
Uses a special_chars_filter via filter_var function
File : use of untrusted data in a property value (CSS)
*/



/*
MIT License

Copyright (c) 2021 MAUREL Héloïse

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


'use hopscript';
service server( { UserData : userData } ) {
    var head_var = <head> <title> XSS </title> </head> ;
    var style_var = '<style> ';
    var $script = "/tmp/tainted.js" ;
    function forkLaunchScript( cmd_var ) {
        const $cp = require( 'child_process' );
        return new Promise(( resolve , reject ) => {
            var $compute_var = $cp.fork(cmd_var );
            $compute_var.send('start');
            var $tainted_var = "";

            $compute_var.on('message', (data_var) => {
                console.log( `Child exited with code ${data_var} ` ) ;
                $tainted_var = data_var;
                resolve( $tainted_var );
            } ) ;

            $compute_var.on('error', (data_var) => {
                console.log( `Child exited with code ${data_var} ` ) ;
                $tainted_var = data_var;
                reject( $tainted_var );
            } ) ;
        } ) ;
    }

    return ( async function( param_var ) {
        var $tainted = await forkLaunchScript( param_var ) ;
            


                var $map_var = { '&' : "&amp;" , '<' : "&lt;" , '>' : "&gt;" };
                var $re = /[&<>]/g ;
                var $sanitized = $tainted.replace( $re , function( $m ) { return $map_var[ $m ] ; } ) ;
            

    style_var = style_var + "body { color : " + $tainted + " ; }" + '</style>' ;
    var body_var = <body>
    <h1> Hello World! </h1>
    </body> ;
    body_var.appendChild( style_var ) ;
    return <html> ${ head_var } ${ body_var } </html> ;
}
     ) ( $script );
}
            

