/* 
Safe sample
input : reads the field UserData from the variable $_GET
Uses a full_special_chars_filter via filter_var function
File : use of untrusted data in a simple quote attribute
*/



/*
MIT License

Copyright (c) 2021 MAUREL Héloïse

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


'use hopscript';
service server( { UserData : userData } ) {
    var head_var = <head> <title> XSS </title> </head> ;
    var body_var = <body> <h1> Hello World! </h1> </body> ;

    var url_var = require( 'url' ) ;
    var q_var = url_var.parse( this.path , true ).query ;
    var $tainted = q_var.UserData ;

        


                function full_special_chars_filter( str_var ) {
                var $map_var = { '&' : "&amp;" , '<' : "&lt;" , '>' : "&gt;" , '"' : "&quot;" , "'" : "&apos;" };
                var $re = /[&<>"']/g ;
                var $sanitized = str_var.replace( $re , function( $m ) { return $map_var[ $m ] ; } ) ;
                return $sanitized ;
                }
                $tainted = full_special_chars_filter( $tainted ) ;
            

    var div_var = '<div id="' + $tainted + '">content</div>' ;
    body_var.appendChild( div_var ) ;
    return <html> ${ head_var } ${ body_var } </html> ;
}