/* 
Safe sample
input : use fopen to read /tmp/tainted.txt and put the first line in $tainted
Uses a full_special_chars_filter via filter_var function
File : unsafe, use of untrusted data in a property value in a span tag(CSS)
*/



/*
MIT License

Copyright (c) 2021 MAUREL Héloïse

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


'use hopscript';
service server( { UserData : userData } ) {
    var head_var = <head> <title> XSS </title> </head> ;
    var body_var = <body> <h1> Hello World! </h1> </body> ;
    var $fs_var = require( 'fs' ) ;
    var $filename = "/tmp/tainted.txt" ;
    var $tainted = $fs_var.readFileSync( $filename , 'utf8' ) ;
        


            function full_special_chars_filter( str_var ) {
                var $map_var = { '&' : "&amp;" , '<' : "&lt;" , '>' : "&gt;" , '"' : "&quot;" , "'" : "&apos;" };
                var $re = /[&<>"']/g ;
                var $sanitized = str_var.replace( $re , function( $m ) { return $map_var[ $m ] ; } ) ;
                return $sanitized ;
            }
            var $sanitized = full_special_chars_filter( $tainted ) ;
            

    body_var.appendChild( "<span style = \"color :" + $tainted + "\"> Hey </span>" ) ;
    return <html> ${ head_var } ${ body_var } </html> ;
}