/* 
Safe sample
input : get the field userData from the variable $_GET via an object, which store it in a array
sanitize : use of ternary condition
File : unsafe, use of untrusted data in a function in a script
*/



/*
MIT License

Copyright (c) 2021 MAUREL Héloïse

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


'use hopscript';
service server( { UserData : userData } ) {
    var head_var = <head> <title> XSS </title> </head> ;
    var script_var = <script/> ;
    var func_var = 'function xss(){';
    var url_var = require('url');
    var path_var = this.path;
    class Input {
        constructor() {
            this.$input = {
                $test : 'safe' ,
                $realOne : url_var.parse( path_var , true).query.userData ,
                $trap : 'safe'
            };
        }

        getInput() {
            return this.$input.$realOne ;
        }
    }
    var $temp = new Input() ;
    var $tainted = $temp.getInput() ;
        

$tainted = $tainted  == 1 + "-1.3e3" ? $tainted : 'safe2' ;

    func_var = func_var + $tainted + '}';
    script_var.appendChild( func_var ) ;
    var body_var = <body onload=~{xss()}>
    <h1> Hello World! </h1>
    </body> ;
    return <html> ${ head_var } ${ script_var } ${ body_var } </html> ;
}
