/* 
Safe sample
input : use shell_exec to cat /tmp/tainted.txt
Uses a number_float_filter via filter_var function
File : use of untrusted data in a div tag
*/



/*
MIT License

Copyright (c) 2021 MAUREL Héloïse

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


'use hopscript';
service server( { UserData : userData } ) {
    var head_var = <head> <title> XSS </title> </head> ;
    var body_var = <body>
    <h1> Hello World! </h1>
    </body> ;
    var div_var = <div/> ;
    function shell_exec_func(cmd_var, param_var ) {
        const $cp = require('child_process') ;
        return new Promise((resolve, reject ) => {
            var $cat_var = $cp.spawn( cmd_var , param_var ) ;
            var $tainted_var = "" ;

            $cat_var.stderr.on('data', ( data_var ) => {
                console.error(data_var.toString());
                reject(data_var);
            });

            $cat_var.on('exit', ( code ) => {
                console.log(`Child exited with code ${ code } `) ;
                resolve($tainted_var);
            });

            $cat_var.stdout.setEncoding('utf8');

            $cat_var.stdout.on('data', function ( data_var ) {
                data_var = data_var.toString() ;
                $tainted_var += data_var ;
            });
        });
    }


    return (async function(){
        var $tainted = await shell_exec_func( 'cat', [ '/tmp/tainted.txt' ] ) ;
            


            function number_float_filter( str_var ) {
                var $all_except_number_re = /[^0-9.\-+]/g ;
                var $sanitized = str_var.replace( $all_except_number_re , '' ) ;
                return $sanitized ;
            }
            var $validation_float = /^(\+|-)?\d+\.\d+$/ ;
            var $sanitized = number_float_filter( $tainted ) ;
            if( $validation_float.test( $sanitized )){
                $tainted = $sanitized ;
            }
            else{
                $tainted = "" ;
            }
            

    div_var.appendChild( $tainted ) ;
    body_var.appendChild( div_var ) ;
    return <html> ${ head_var } ${ body_var } </html> ;
}
          )() ;
}
            

