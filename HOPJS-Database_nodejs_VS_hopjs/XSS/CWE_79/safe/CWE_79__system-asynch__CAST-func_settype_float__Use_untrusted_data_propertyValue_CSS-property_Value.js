/* 
Safe sample
input : execute a ls command using the function system, and put the last result in $tainted
sanitize : settype (float)
File : use of untrusted data in a property value (CSS)
*/



/*
MIT License

Copyright (c) 2021 MAUREL Héloïse

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


'use hopscript';
service server( { UserData : userData } ) {
    var head_var = <head> <title> XSS </title> </head> ;
    var style_var = '<style> ';
    function system_func( cmd_var ) {
        const exec = require('child_process').exec ;
        return new Promise( (resolve , reject ) => {
            exec( cmd_var , ( error_var, stdout_var, stderr_var ) => {
                if ( error_var ) {
                    console.warn( error_var );
                    reject( error_var ) ;
                }
                resolve( stdout_var ? stdout_var : stderr_var ) ;
            });
        });
    }
    return (async function( param_var ) {
        var $tainted = await system_func( param_var );
            


            $tainted = parseFloat( $tainted ) ;
            if ( typeof( $tainted ) == "float" ){
                $tainted = $tainted ;
            }
            else {
                $tainted = 0.0 ;
            }
            

    style_var = style_var + "body { color : " + $tainted + " ; }" + '</style>' ;
    var body_var = <body>
    <h1> Hello World! </h1>
    </body> ;
    body_var.appendChild( style_var ) ;
    return <html> ${ head_var } ${ body_var } </html> ;
}
        )( "ls" );
}
    

