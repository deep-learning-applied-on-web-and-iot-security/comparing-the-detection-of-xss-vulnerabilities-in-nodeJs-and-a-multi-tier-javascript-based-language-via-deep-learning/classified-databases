/* 
Safe sample
input : Uses popen to read the file /tmp/tainted.txt using cat command
Uses a number_int_filter via filter_var function
File : use of untrusted data in a property value (CSS)
*/



/*
MIT License

Copyright (c) 2021 MAUREL Héloïse

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


'use hopscript';
service server( { UserData : userData } ) {
    var head_var = <head> <title> XSS </title> </head> ;
    var style_var = '<style> ';
    function popen_func(cmd){
        const $cp = require( 'child_process' );
        return new Promise(( resolve, reject ) => {
            $cp.exec(cmd, (error_var, stdout_var, stderr_var) => {
                if (error_var) {
                    console.warn( error_var );
                    reject( error_var ) ;
                }
                resolve(stdout_var ? stdout_var : stderr_var);
            });
        });


    }
    return (async function(){
        var $tainted = await popen_func( '/bin/cat /tmp/tainted.txt' ) ;
            


    function number_int_filter( str_var ) {
        var $all_except_number_re = /[^0-9-+]/g ;
        var $sanitized = str_var.replace( $all_except_number_re , '' ) ;
        return $sanitized ;
    }

    var $sanitized = number_int_filter( $tainted ) ;
    var $numbers_re = /^[+-]?\d+$/g ;
    if( $numbers_re.test( $sanitized )){
        $tainted = $sanitized ;
    }
    else{
        $tainted = "" ;
    }
            

    style_var = style_var + "body { color : " + $tainted + " ; }" + '</style>' ;
    var body_var = <body>
    <h1> Hello World! </h1>
    </body> ;
    body_var.appendChild( style_var ) ;
    return <html> ${ head_var } ${ body_var } </html> ;
}
    )() ;
}
            

